/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author mathe
 */
public class FXMLInicialController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public void paraABatalha(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLTelaInicial.fxml");
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}

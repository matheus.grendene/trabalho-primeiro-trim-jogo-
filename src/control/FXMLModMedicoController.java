/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;


import static control.TrabalhoTrim1.listaM;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import trabalhotrim1.Medico;

/**
 * FXML Controller class
 *
 * @author mathe
 */
public class FXMLModMedicoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    Medico med = new Medico();
    @FXML
    private Label quantAtual;
    @FXML
    private TextField quant;
    @FXML
    private Label atkAtual;
    
    @FXML
    private void voltar(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLTelaInicial.fxml");
    }
    @FXML
    private void quantAply(ActionEvent event)
    {
        med = (Medico) listaM.get(0);
        med.setQuant(Integer.parseInt(quant.getText()));
        quantAtual.setText(med.getQuant() + " Unidades");
        listaM.add(0,med); 
    }
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        med = (Medico) listaM.get(0);
        quantAtual.setText(med.getQuant() + " Unidades");
        atkAtual.setText("Não contem ataque padrão");
    }   
    
}
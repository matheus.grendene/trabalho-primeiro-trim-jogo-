/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhotrim1;

/**
 *
 * @author mathe
 */
public class Medico extends Soldados{
    private int vida;
    private int ataque;
    public int quant;
    
    public Medico() {
        this.vida = 50;
        this.ataque = 0;
        this.quant = 1;
    }

    //getters e setters
    
    public int getCura() {
        return ataque;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }
    
    public int getQuant() {
        return quant;
    }

    public void setQuant(int quant) {
        this.quant = quant;
    }

    //metodos
    public int getFullVida()
    {
        return getVida()*getQuant();
    }
    public int cura() {
        return (getQuant()/10)*getCura();//como o esta unidade é uma unidade medica ela não ataca normalmente, ela cura
    }
    
    //Overrides (do Abstract)
    @Override
    public int especial()
    {//PRECISÃO CIRUGICA: da hit kill em qualquer unidade (exeto artilharia)
        return 80;
    }
    @Override
    public int ataca() {
        return 0;
        //ainda esta aqui porém não será utilizada
    }
    @Override
    public void tomarDano(int dano) {
        this.vida -= dano;
    }
}
